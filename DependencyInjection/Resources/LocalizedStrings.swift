//
//  LocalizedStrings.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

enum LocalizedEnum: String {
    case earthquakeTitle = "earthquakeTitle"
}

final class LocalizedStrings {
    
    static func getLocalizedString(key: LocalizedEnum, isTr: Bool) -> String {
        switch key {
        case .earthquakeTitle:
            return isTr ? "Depremler" : "Earthquakes"
        }
    }
}

