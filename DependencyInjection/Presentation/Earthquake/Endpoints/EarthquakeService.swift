//
//  Earthquake.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

enum EarthquakeService {
    case getEarthquake(parameters: EarthquakeRequest)
}

extension EarthquakeService: ConnectionManager {
    
    var baseURL: String {
        return "https://apis.is/earthquake/is"
    }

    var parameters: EarthquakeRequest? {
        var params: EarthquakeRequest?
        
        switch self {
            case .getEarthquake(let dictionary):
                params = dictionary
        }
        return params
    }

    var method: ServiceMethod {
        return .get
    }
}

final class EarthquakeRequest: BaseEncodableModel {
    var selectedCountry = ""
}

final class EarthquakeResponse: BaseDecodableModel {
    
    var results = [EarthquakeModel]()

    private enum CodingKeys: String, CodingKey {
        case results
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
        let values = try decoder.container(keyedBy: CodingKeys.self)
        results = try values.decode([EarthquakeModel].self, forKey: .results)
    }
}
