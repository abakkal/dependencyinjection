//
//  ViewController.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import UIKit

final class EarthquakeVC: BaseViewController {

    // MARK: - OUTLETS
    
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - PROPERTIES
    
    var viewModel: EarthquakeVM?
    
    // MARK: - INITS
    
    init(_ viewModel: EarthquakeVM) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - LIFE CYCLE

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableView()
        prepareViewModel()
    }
    
    private func prepareTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.registerCell(type: EarthquakeCell.self)
    }
    
    private func prepareViewModel() {
        guard let viewModel = viewModel else { return }
        viewModel.delegate = self
        viewModel.getEarthquake()
    }
}

// MARK: - EXTENSIONS

extension EarthquakeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension EarthquakeVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.getEarthquakeItemCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(withType: EarthquakeCell.self, for: indexPath) as? EarthquakeCell,
        let earthquakeItem = viewModel?.getEarthquakeItem(at: indexPath.row) else { return UITableViewCell() }
    
        // Constructor Injection
        
        let earthquakeCellVM = EarthquakeCellVM(earthquakeItem)
        
        // Property Injection
        
        cell.viewModel = earthquakeCellVM

        // Method Injection

        if indexPath.row % 3 == 0 {
            cell.setStyler(TRStyle())
        } else if indexPath.row % 3 == 1 {
            cell.setStyler(BJKStyle())
        } else {
            cell.setStyler(FBStyle())
        }
        
        return cell
    }
}

extension EarthquakeVC: EarthquakeVMProtocol {
    func responseSucceeded() {
        tableView.reloadData()
    }
    
    func responseFailed(_ error: Error) {
        print(error)
    }
}
