//
//  EarthquakeCellVM.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

final class EarthquakeCellVM: BaseUiVM {
    
    var latitude: String?
    var longitude: String?
    var size: String?
    var humanReadableLocation: String?
    
    init(_ earthquakeItem: EarthquakeModel) {
        self.latitude = String(earthquakeItem.latitude)
        self.longitude = String(earthquakeItem.longitude)
        self.size =  String(earthquakeItem.size)
        self.humanReadableLocation = earthquakeItem.humanReadableLocation
    }
}
