//
//  EarthquakeVM.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

protocol EarthquakeVMProtocol: BaseProtocol {
    func responseSucceeded()
    func responseFailed(_ error: Error)
}

final class EarthquakeVM: BaseControllerVM {
    
    // MARK: - PROPERTIES
    
    weak var delegate: EarthquakeVMProtocol?
    private var earthquakeList: [EarthquakeModel] = []

    // MARK: - CUSTOM FUNCTIONS
    
    func getEarthquake() {
        let earthquakeProvider = ConnectionManagerProvider<EarthquakeService>()
        let request = EarthquakeRequest()
        earthquakeProvider.load(service: .getEarthquake(parameters: request), decodeType: EarthquakeResponse.self) { result in
            switch result {
            case .success(let response):
                self.earthquakeList = response.results
                self.delegate?.responseSucceeded()
            case .failure(let error):
                self.delegate?.responseFailed(error)
            case .empty:
                break
            }
        }
    }
    
    func getEarthquakeItem(at index: Int) -> EarthquakeModel {
        return earthquakeList[index]
    }
    
    func getEarthquakeItemCount() -> Int {
        return earthquakeList.count
    }
}
