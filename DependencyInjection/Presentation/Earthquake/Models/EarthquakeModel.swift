//
//  EarthquakeModel.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

final class EarthquakeModel: Codable {
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var size: Double = 0.0
    var humanReadableLocation: String = ""
}
