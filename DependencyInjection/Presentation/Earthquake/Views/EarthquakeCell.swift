//
//  EarthquakeCell.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//


import UIKit

final class EarthquakeCell: UITableViewCell {

    // MARK: - OUTLETS
    
    @IBOutlet private weak var latitude: UILabel!
    @IBOutlet weak var latitudeTitle: UILabel!
    @IBOutlet private weak var longitude: UILabel!
    @IBOutlet weak var logitudeTitle: UILabel!
    @IBOutlet private weak var size: UILabel!
    @IBOutlet private weak var humanReadableLocation: UILabel!
    
    // MARK: - PROPERTIES
    
    var viewModel: EarthquakeCellVM? {
        didSet {
            setCell()
        }
    }
    
    var styler: Styler? {
        didSet {
            setStyle()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    // MARK: - SET CELL
    
    private func setCell() {
        guard let viewModel = viewModel else { return }
        latitude.text = viewModel.latitude
        longitude.text = viewModel.longitude
        size.text = viewModel.size 
        humanReadableLocation.text = viewModel.humanReadableLocation
    }
    
    func setStyle() {
        guard let styler = styler else { return }
        backgroundColor = styler.getPrimaryColor()

        let textColor = styler.getTextColor()
        let textFont = UIFont.systemFont(ofSize: styler.getTextSize())
        
        latitude.textColor = textColor
        longitude.textColor = textColor
        latitudeTitle.textColor = textColor
        logitudeTitle.textColor = textColor
        size.textColor = textColor
        humanReadableLocation.textColor = textColor
        
        latitude.font = textFont
        longitude.font = textFont
        latitudeTitle.font = textFont
        logitudeTitle.font = textFont
        size.font = textFont
        humanReadableLocation.font = textFont
    }
    
    func setStyler(_ styler: Styler) {
        self.styler = styler
    }
}
