//
//  InitialVC.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import UIKit

final class InitialVC: BaseViewController {
    
    // MARK: - OUTLETS
    
    @IBOutlet private weak var getEarthquake: UIButton!
        
    // MARK: - ACTIONS
    
    @IBAction private func buttonTouched(_ sender: Any) {
        
        // Constructor Injection
        
        let earthquakeVM = EarthquakeVM()
        let earthquakeVC = EarthquakeVC(earthquakeVM)
        
        presentToTopViewController(earthquakeVC)
    }    
}
