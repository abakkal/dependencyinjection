//
//  EncodableExtension.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
