//
//  UIViewControllerExtensions.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 3.06.2021.
//

import UIKit

extension BaseViewController {
    func rootViewController() -> BaseViewController? {
        guard let root = UIApplication.shared.windows.first?.rootViewController as? BaseViewController else { return nil }
        return root
    }
    
    func presentToTopViewController(_ controller: BaseViewController) {
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .pageSheet
        rootViewController()?.present(navigationController, animated: true, completion: nil)
    }
}
