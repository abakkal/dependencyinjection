//
//  Styler.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import UIKit

protocol Styler: class {
    func getPrimaryColor() -> UIColor
    func getTextColor() -> UIColor
    func getTextSize() -> CGFloat
}

final class GSStyle: Styler {
    func getPrimaryColor() -> UIColor {
        return .yellow
    }
    
    func getTextColor() -> UIColor {
        return .red
    }
    
    func getTextSize() -> CGFloat {
        return 15
    }
}

final class BJKStyle: Styler {
    func getPrimaryColor() -> UIColor {
        return .black
    }
    
    func getTextColor() -> UIColor {
        return .white
    }
    
    func getTextSize() -> CGFloat {
        return 20
    }
}

final class FBStyle: Styler {
    func getPrimaryColor() -> UIColor {
        return .blue
    }
    
    func getTextColor() -> UIColor {
        return .yellow
    }
    
    func getTextSize() -> CGFloat {
        return 25
    }
}

final class TRStyle: Styler {
    func getPrimaryColor() -> UIColor {
        return .red
    }
    
    func getTextColor() -> UIColor {
        return .white
    }
    
    func getTextSize() -> CGFloat {
        return 50
    }
}

