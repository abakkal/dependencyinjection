//
//  Assembler.swift
//  DependencyInjection
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

protocol Assembler: class {
    func registerComponents()
}
