//
//  ConnectionManager.swift
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation
import UserNotifications

protocol ConnectionManager {
    associatedtype EncodableElement
    var baseURL: String { get }
    var parameters: EncodableElement? { get }
    var method: ServiceMethod { get }
}

extension ConnectionManager {
    public var urlRequest: URLRequest {
        
        // Prepare request
        var request = URLRequest(url: URL(string: baseURL)!)
        request.httpMethod = method.rawValue
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        if method != .get {
            if let parameterDictionary = (parameters as? BaseEncodableModel).dictionary {
                print("\n ----------------Request---------------- ")
                print("\n BaseURL: " + baseURL, "\n")
                print(parameterDictionary.debugDescription)
                let postData = try! JSONSerialization.data(withJSONObject: parameterDictionary, options: .prettyPrinted)
                request.httpBody = postData as Data
            }
        }
        return request as URLRequest
    }
}
