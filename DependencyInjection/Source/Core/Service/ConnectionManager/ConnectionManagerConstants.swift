//
//  ConnectionManagerConstants.swift
//
//  Created by AHMET BEKIR BAKKAL on 2.06.2021.
//

import Foundation

enum ServiceMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

struct ContentType {
    static let applicationJson = "application/json"
}

struct ServiceStatusCodes {
    static let success = 0;
    static let success1 = 200;
    static let loginFailed = 201;
    static let invalidToken = 202;
    static let changePassword = 203;
    static let unknownError = 204;
    static let processError = 205;
    static let missingParameter = 206;
}
